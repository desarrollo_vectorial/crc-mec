<footer>

    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h2>ORGANIZA:</h2>
                <div class="col-md-4 p0">
                    <img src="<?php bloginfo('template_url') ?>/assets/images/logo-crc.png" class="logo-crc img-responsive logo-responsive-footer">
                </div>
                <div class="col-md-8 p0">
                    <img src="<?php bloginfo('template_url') ?>/assets/images/logo-ccc.png" class="logo-ccc img-responsive logo-responsive-footer">
                </div>
            </div>
            <div class="col-md-9">
                <h2>ALIADOS:</h2>
                <div class="col-md-4 p0">
                    <img src="<?php bloginfo('template_url') ?>/assets/images/logo-valle.png" class="img-responsive logo-responsive-footer">
                </div>
                <div class="col-md-2 p0">
                    <img src="<?php bloginfo('template_url') ?>/assets/images/logo-alcaldia.png" class="img-responsive logo-responsive-footer">
                </div>
                <div class="col-md-3 p0">
                    <img src="<?php bloginfo('template_url') ?>/assets/images/logo-calicreativa.png" class="img-responsive logo-responsive-footer">
                </div>
            </div>
        </div>
    </div>

</footer>

<?php

$id_video = get_field( "id_del_video", "option" );

?>



<div class="custom-modal">
    <div class="modal fade" id="ModalVideo" tabindex="-1" role="dialog" aria-labelledby="ModalVideoLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="fa fa-close"></span></button>
                <div class="modal-body p0">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="//www.youtube.com/embed/<?php if ($id_video): ?><?php echo $id_video; ?><?php else: ?>y7ivJklVVDY<?php endif; ?>" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="customoverlay"></div>

<?php wp_footer(); ?>
</body>
</html>