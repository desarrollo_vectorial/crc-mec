<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' |'; } ?> <?php bloginfo('name'); ?> <?php bloginfo('description'); ?></title>
        <link rel="shortcut icon" type="image/png" href="<?php bloginfo('template_url') ?>/assets/images/favicon.png"/>
        <meta name="description" content="MEC, un evento para el crecimiento de las industrias creativas de la región, cuyo objetivo es brindarle a los empresarios de Cali y el Valle del Cauca herramientas que les permitan fortalecer su modelo de negocio, conectarse con otros creativos y crecer.">
        <?php html5blank_styles(); ?>
        <?php html5blank_header_scripts(); ?>
        <?php wp_head(); ?>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127302123-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-127302123-1');
        </script>

</head>
<body <?php body_class(); ?>>

<?php

$logo_mec = get_field( "logo_mec", "option" );
$activar_video = get_field( "activar_boton_video", "option" );
$id_video = get_field( "id_del_video", "option" );
$activar_agenda = get_field( "activar_boton_agenda", "option" );
$url_agenda = get_field( "agenda_en_pdf", "option" );
$btn_suscripcion = get_field( "activar_boton_de_suscripcion", "option" );

?>

<header>
    <nav class="navbar navbar-custom">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <?php if ($logo_mec) : ?>
                    <a class="navbar-brand" href="/"><img src="<?php echo $logo_mec; ?>"></a>
                <?php else: ?>
                    <a class="navbar-brand" href="/"><img src="<?php bloginfo('template_url') ?>/assets/images/logo-cabezote.png"></a>
                <?php endif; ?>
                <ul class="nav navbar-nav pull-right mr5 hide-on-d">
                    <?php if ($activar_video == "SI" ) : ?>
                        <li class="pull-left"><a href="#" data-toggle="modal" data-target="#ModalVideo"><span class="mec-ico-video"></span></a></li>
                    <?php else: ?><?php endif; ?>
                    <?php if ($activar_agenda == "SI" ) : ?>
                        <li class="pull-left">
                            <?php if ($url_agenda) : ?>
                            <a href="<?php echo $url_agenda; ?>" target="_blank">
                                <span class="fa fa-cloud-download"></span>
                            </a>
                            <?php else: ?>
                            <a href="https://www.ccc.org.co/wp-content/uploads/2018/09/mec_agenda.pdf" target="_blank">
                                <span class="fa fa-cloud-download"></span>
                            </a>
                            <?php endif; ?>
                        </li>
                    <?php else: ?><?php endif; ?>
                    <?php if ($btn_suscripcion == "SI" ) : ?>
                        <li class="pull-left"><a href="#" class="js-open" data-target="#nav-sidebar" aria-controls="nav-sidebar"><span class="fa fa-align-right"></span></a></li>
                    <?php else: ?><?php endif; ?>
                </ul>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div>
                <ul class="nav navbar-nav navbar-right pr90 hide-on-m">
                    <?php if ($activar_video == "SI" ) : ?>
                        <li><a href="#" data-toggle="modal" data-target="#ModalVideo"><span class="mec-ico-video"></span> VIDEO</a></li>
                    <?php else: ?><?php endif; ?>
                    <?php if ($activar_agenda == "SI" ) : ?>
                        <li>
                            <?php if ($url_agenda) : ?>
                                <a href="<?php echo $url_agenda; ?>" target="_blank">DESCARGA LA AGENDA</a>
                            <?php else: ?>
                                <a href="http://crcvalle.org.co/mec/wp-content/uploads/2018/10/mec_agenda-1.pdf" target="_blank">DESCARGA LA AGENDA</a>
                            <?php endif; ?>
                        </li>
                    <?php else: ?><?php endif; ?>
                    <?php if ($btn_suscripcion == "SI" ) : ?>
                        <li><a href="#" class="solid js-open" data-target="#nav-sidebar" aria-controls="nav-sidebar">INSCRÍBETE</a></li>
                    <?php else: ?><?php endif; ?>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

    <div class="container_leftnavbar">
        <div class="__leftnavbar">
            <ul id="leftnavbar">
                <li data-menuanchor="page1" class="active">
                    <a href="#page1" class="mec-ico-descripcion"></a>
                    <p><strong>Descripción</strong><br>MEC</p>
                </li>
                <li data-menuanchor="page2">
                    <a href="#page2" class="mec-ico-resultados"></a>
                    <p><strong>Resultados</strong><br>MEC 2017</p>
                </li>
                <li data-menuanchor="page3">
                    <a href="#page3" class="mec-ico-espacios"></a>
                    <p><strong>Espacios</strong></p>
                </li>
                <li data-menuanchor="page4">
                   <a href="#page4" class="mec-ico-sectores"></a>
                    <p><strong>SECTORES</strong><br>PARTICIPANTES MEC 2018</p>
               </li>
            </ul>
        </div>
    </div>
    <div class="off-canvas-panel" id="nav-sidebar">

        <div class="sidebar-content p40 text-center">
            <h2 class="mb40"><span class="close_sidebar"><i class="fa fa-chevron-left" aria-hidden="true"></i></span> INSCRÍBETE</h2>
            <div class="formulario">
                <?php echo do_shortcode('[contact-form-7 id="5" title="Contact form 1"]'); ?>
            </div>
        </div>
    </div>
</header>