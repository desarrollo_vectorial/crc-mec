<?php get_header(); wp_head();?>
    <div class="container-fluid">
        <div class="row">
            <section id="horizontal-scroll">
                <div class="container-fluid container-custom section pl0 pr0" id="section1">
                    <?php include get_template_directory() . "/fragments/section1.php"; ?>
                </div>
                <div class="container-fluid container-custom section pl0 pr0" id="section2">
                    <?php include get_template_directory() . "/fragments/section2.php"; ?>
                </div>
                <div class="container-fluid container-custom section pl0 pr0" id="section3">
                    <?php include get_template_directory() . "/fragments/section3.php"; ?>
                </div>
                <div class="container-fluid container-custom section pl0 pr0" id="section4">
                    <?php include get_template_directory() . "/fragments/section4.php"; ?>
                </div>
            </section>
        </div>
    </div>
<?php wp_footer(); get_footer(); ?>