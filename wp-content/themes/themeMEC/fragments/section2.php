<div class="row ml0 mr0">
    <div class="col-md-12 banner_2 pt30 pb30 pl90 pr90">
        <div class="owl-carousel owl-theme">
            <div class="item">
                <div class="col-md-12 text-center cifras">
                    <i class="mec-cifras-bandas"></i>
                    <p class="num">28</p>
                    <p class="title">BANDAS</p>
                </div>
            </div>
            <div class="item">
                <div class="col-md-12 text-center cifras">
                    <i class="mec-cifras-artistas"></i>
                    <p class="num">17</p>
                    <p class="title">ARTISTAS</p>
                </div>
            </div>
            <div class="item">
                <div class="col-md-12 text-center cifras">
                    <i class="mec-cifras-speakers"></i>
                    <p class="num">120+</p>
                    <p class="title">SPEAKERS</p>
                </div>
            </div>
            <div class="item">
                <div class="col-md-12 text-center cifras">
                    <i class="mec-cifras-peliculas"></i>
                    <p class="num">7</p>
                    <p class="title">PELÍCULAS</p>
                </div>
            </div>
            <div class="item">
                <div class="col-md-12 text-center cifras">
                    <i class="mec-firmas-moda"></i>
                    <p class="num">10</p>
                    <p class="title">FIRMAS DE MODA</p>
                </div>
            </div>
            <div class="item">
                <div class="col-md-12 text-center cifras">
                    <i class="mec-emprendimiento"></i>
                    <p class="num">20</p>
                    <p class="title">EMPRENDEDORES</p>
                </div>
            </div>
            <div class="item">
                <div class="col-md-12 text-center cifras">
                    <i class="mec-aliado"></i>
                    <p class="num">30</p>
                    <p class="title">ALIADOS</p>
                </div>
            </div>
            <div class="item">
                <div class="col-md-12 text-center cifras">
                    <i class="mec-talleres"></i>
                    <p class="num">11</p>
                    <p class="title">TALLERES</p>
                </div>
            </div>
            <div class="item">
                <div class="col-md-12 text-center cifras">
                    <i class="mec-charlas"></i>
                    <p class="num">17</p>
                    <p class="title">CHARLAS</p>
                </div>
            </div>
            <div class="item">
                <div class="col-md-12 text-center cifras">
                    <i class="mec-paneles"></i>
                    <p class="num">14</p>
                    <p class="title">PANELS</p>
                </div>
            </div>
            <div class="item">
                <div class="col-md-12 text-center cifras">
                    <i class="mec-mesa-de-trabajo"></i>
                    <p class="num">1</p>
                    <p class="title">MESA DE TRABAJO</p>
                </div>
            </div>
        </div>
        <div class="text-center text-registro">
            <div class="col-md-6">
                <p><strong>2861</strong> REGISTRADOS</p>
            </div>
            <div class="col-md-6">
                <p><strong>5500+</strong> ASISTENCIAS</p>
            </div>
        </div>
    </div>
</div>