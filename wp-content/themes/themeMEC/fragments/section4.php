<div class="row ml0 mr0">
    <div class="col-md-12 banner_4 p0">
        <div class="cd-projects-container">
            <ul class="cd-projects-previews">
                <li>
                    <a href="#0">
                        <div class="cd-project-title">
                            <h2>MÚSICA</h2>
                            <p>6TO MERCADO MUSICAL DEL PACÍFICO</p>
                        </div>
                    </a>
                </li>

                <li>
                    <a href="#0">
                        <div class="cd-project-title">
                            <h2>TECNOLOGÍA</h2>
                            <!--<p>Brief description of the project here</p>-->
                        </div>
                    </a>
                </li>

                <li>
                    <a href="#0">
                        <div class="cd-project-title">
                            <h2>SISTEMA MODA</h2>
                            <!--<p>Brief description of the project here</p>-->
                        </div>
                    </a>
                </li>

                <li>
                    <a href="#0">
                        <div class="cd-project-title">
                            <h2>ARTE</h2>
                            <!--<p>Brief description of the project here</p>-->
                        </div>
                    </a>
                </li>

                <li>
                    <a href="#0">
                        <div class="cd-project-title">
                            <h2>AUDIOVISUAL</h2>
                            <!--<p>Brief description of the project here</p>-->
                        </div>
                    </a>
                </li>
            </ul> <!-- .cd-projects-previews -->
            <ul class="cd-projects">
                <a class="cd-nav-trigger cd-text-replace" href="#primary-nav">Menu<span aria-hidden="true" class="cd-icon"></span></a>
                <li>
                    <div class="preview-image">
                        <div class="cd-project-title">
                            <h2>MÚSICA</h2>
                            <p>6TO MERCADO MUSICAL DEL PACÍFICO</p>
                        </div>
                    </div>
                    <div class="cd-project-info">
                        <div class="container info">
                            <div class="row">
                                <div class="col-md-12">
                                    <h2>OBJETIVOS</h2>
                                    <p>Conectar los grupos musicales del Pacífico Colombiano con las dinámicas de mercado y los circuitos nacionales e internacionales.</p>
                                    <h2>INVITADOS</h2>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Mary Núñez</p>
                                                        <p>(VP Synchs Sony Music Latin Iberia)</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Paul Brindley</p>
                                                        <p>(CEO Music Ally Londres)</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 ">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Scott Cohen</p>
                                                        <p>(VP The Orchard Londres)</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Eduardo Cabra</p>
                                                        <p>(Puerto Rico)</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 ">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Andrés Sánchez</p>
                                                        <p>(México)</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Salvador Toache</p>
                                                        <p>(México)</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 ">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Luis Arias</p>
                                                        <p>(Costa Rica)</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Edgardo Villanueva Málaga</p>
                                                        <p>(Perú)</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 ">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Giovanny Barrientos</p>
                                                        <p>(Panamá)</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Ricardo Saavedra</p>
                                                        <p>(Chile)</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 ">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Alejandro Tavares</p>
                                                        <p>(México)</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Jorge Asanza</p>
                                                        <p>(Ecuador)</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 ">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Camilo Carrillo</p>
                                                        <p>(Bucaramanga)</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Chucky García</p>
                                                        <p>(Bogotá)</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 ">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Iván Oliva</p>
                                                        <p>(Pasto)</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Daniel Buitrago</p>
                                                        <p>(Medellín)</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 ">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Sergio Pabón</p>
                                                        <p>(Bogotá)</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Felipe Vallejo</p>
                                                        <p>(Bogotá)</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 ">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Angélica Jaramillo</p>
                                                        <p>(Barranquilla)</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Steven Sierra</p>
                                                        <p>(Bogotá)</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 ">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">IDARTES</p>
                                                        <p>(Bogotá)</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Manolo Arango</p>
                                                        <p>(Medellín)</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 ">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Sofía Villar</p>
                                                        <p>(Bogotá)</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">+ 16 compradores regionales</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <h2>CONTENIDO</h2>
                                        <div class="col-md-12">
                                            <div><strong><p>Agenda Académica</p></strong></div>
                                            <div><strong><p>12 showcases</p></strong></div>
                                            <div><strong><p>Rueda de negocios</p></strong></div>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <h2>AGENDA ACADÉMICA</h2>
                                        <div class="col-md-6">
                                            <h3><strong>TALLERES</strong></h3>
                                            <div><strong>Laboratorio de producción en Pro Tools</strong></div>
                                            <div><strong>Taller de audio en estudio aplicado a sonido en vivo</strong></div>
                                        </div>
                                        <div class="col-md-6">
                                            <h3><strong>CHARLAS</strong></h3>
                                            <div><strong>Estrategias de circulación nacional e internacional </strong></div>
                                            <div><strong>Agrupaciones musicales y marcas </strong></div>
                                            <div><strong>Publishing en la música </strong></div>
                                            <div><strong>Derechos de autor</strong></div>
                                            <div><strong>Producción moderna y la importancia de la canción</strong></div>
                                            <div><strong>Explotación de música para sincronizaciones</strong></div>
                                            <div><strong>Derechos editoriales, su administración y explotación</strong></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- .cd-project-info -->
                </li>
                <li>
                    <div class="preview-image">
                        <div class="cd-project-title">
                            <h2>TECNOLOGÍA</h2>
                            <!--<p>Brief description of the project here</p>-->
                        </div>
                    </div>
                    <div class="cd-project-info">
                        <div class="container info">
                            <div class="row">
                                <div class="col-md-12">
                                    <h2>OBJETIVOS</h2>
                                    <p>Exponer las nuevas tendencias en el sector tecnológico a través de diferentes herramientas que permitan fortalecer su modelo de negocio.</p>
                                    <h2>INVITADOS</h2>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/Rosario-Ballesteros-Casas.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Rosario Ballesteros Casas</p>
                                                        <p>Empresaria colombiana radicada en Nueva York con un exitoso historial en originación, estructuración y ejecución de transacciones en plataformas de datos y tecnológicas. Cofundadora de VR Américas, una compañía que trabaja en tecnologías inmersivas - Realidad Virtual y Aumentada, y ha sido seleccionada por el WXR Fund, 4.0 Schools, y NYU Steinhardt StartEd Incubator.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="userCards" style="min-height: 278px;">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/Edwin-Zacipa.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Edwin Zácipa</p>
                                                        <p>Director ejecutivo de Colombia Fintech. Actualmente, lidera la dirección ejecutiva de la Asociación Colombia Fintech. Hoy es el joven más influyente del ecosistema Fintech de Iberoamérica, según el Fintech Influencer Ranking de la consultora española Finnovating.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 ">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/Javier-Cardona.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Javier Cardona </p>
                                                        <p>Emprendedor, Co-fundador y CEO de 1DOC3. Administrador de empresas de la universidad externado de Colombia y Master en Marketing de la universidad EAFIT. Cuenta con más de 18 años de experiencia en la industria de telecomunicaciones e internet alrededor de América Latina, Medio Oriente y Africa.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="userCards" style="min-height: 258px;">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/sebastian.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Sebastián Jasminoy</p>
                                                        <p>Fundador y CEO de FLUVIP, el grupo de marketing de influenciadores con mayor presencia en el mundo. Él es considerado un pionero y un experto dentro de la industria de Influencer Marketing</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/cesar.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Cesar Aya</p>
                                                        <p>Desde hace 5 años es inversionista en start-ups de tecnología, su pasión es aprender sobre tecnologías que pueden impactar profundamente el mundo y superar los retos de llevar esas nuevas tecnologías a los mercados corporativos que aun no están listos para adoptarlas.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Liliana Gómez</p>
                                                        <p>(Green SQA)</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <h2>CONTENIDO</h2>
                                        <div class="col-md-12">
                                            <div><strong>Agenda Académica</strong></div>
                                            <div><strong>Mentor Café</strong></div>
                                            <div><strong>Foguéate</strong></div>
                                            <div><strong>Linkéate</strong></div>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <h2>AGENDA ACADÉMICA</h2>
                                        <div class="col-md-6">
                                            <h3><strong>TALLERES</strong></h3>
                                            <div><strong>Experiencias de tecnologías inmersivas  </strong></div>
                                            <div><strong>Financiamiento en la Tecnología  </strong></div>
                                            <div><strong>Cómo levantar la inversión de Fondos de Capital </strong></div>
                                            <div><strong>Innovación a la hora de comunicar</strong></div>
                                        </div>
                                        <div class="col-md-6">
                                            <h3><strong>CHARLAS</strong></h3>
                                            <div><strong>Caso empresarial caleño  </strong></div>
                                            <div><strong>Caso empresarial nacional </strong></div>
                                            <div><strong>Experiencias de tecnologías inmersivas </strong></div>
                                            <div><strong>Big Data Analytics & Artificial Intelligence</strong></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- .cd-project-info -->
                </li>
                <li>
                    <div class="preview-image">
                        <div class="cd-project-title">
                            <h2>SISTEMA MODA</h2>
                            <!--<p>Brief description of the project here</p>-->
                        </div>
                    </div>
                    <div class="cd-project-info">
                        <div class="container info">
                            <div class="row">
                                <div class="col-md-12">
                                    <h2>OBJETIVOS</h2>
                                    <p>Visibilizar y fortalecer las propuestas de diseño de las empresas del  Cluster Sistema Moda como expresión de la economía creativa de la región.</p>
                                    <p>Ofrecer espacios para la formación especializada del capital humano de la industria de la región.</p>

                                    <h2>INVITADOS</h2>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/saul_foto.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Saúl Valero</p>
                                                        <p>Administrador de Empresas con especialidad en Gerencia de Mercadeo. Se ha desempeñado como asesor y conferencista para compañías como Adidas, Zara, Carlos Nieto entre otras.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/ana_lucia.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Ana Lucía Jaramillo</p>
                                                        <p>Consultora con 18 años de experiencia en temas de arquitectura de marca, diseño y mercadeo. Ha trabajado con empresas como Andrés Otalora, Renata Lozano, Calzatodo entre otras.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <h2>CONTENIDO</h2>
                                        <div class="col-md-12">
                                            <div><strong>4 Charlas</strong></div>
                                            <div><strong>1 Showroom</strong></div>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <h2>AGENDA ACADÉMICA</h2>
                                        <h3>Jueves 29 de noviembre</h3>
                                        <p>9:00 a.m. – 10:00 a.m.</p>
                                        <p>Emprendimiento para la moda: de diseñador a empresario</p>
                                        <p><strong>Saúl Valero</strong></p>
                                        <p>10:30 a.m. – 11:30 a.m.</p>
                                        <p>Arquitectura de marca: El corazón de una marca son sus clientes</p>
                                        <p><strong>Ana Lucía Jaramillo</strong></p>
                                        <h3>Viernes 30 de noviembre</h3>
                                        <p>9:00 a.m. – 10:00 a.m</p>
                                        <p>Distribución: Un mundo de posibilidades</p>
                                        <p><strong>Lluis Miracle</strong></p>
                                        <p>10:30 a.m. – 11:30 a.m.</p>
                                        <p>Caso de éxito: Totto innovación local con visión global</p>
                                        <p><strong>Paulo Felipe Quintero</strong></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- .cd-project-info -->
                </li>
                <li>
                    <div class="preview-image">
                        <div class="cd-project-title">
                            <h2>ARTE</h2>
                            <!--<p>Brief description of the project here</p>-->
                        </div>
                    </div>
                    <div class="cd-project-info">
                        <div class="container info">
                            <div class="row">
                                <div class="col-md-12">
                                    <h2>OBJETIVOS</h2>
                                    <p>Objetivo: Conectar a través de circulación, exposición y ventas a los artistas plásticos del Pacífico colombiano con coleccionistas, galeristas y curadores de Bogotá y otros países.</p>

                                    <a href="<?php bloginfo('template_url') ?>/assets/documents/TERMINOS-DE-REFENCIA-Salon-Pacifico-2018.pdf" class="btn btns btn-lg" target="_blank">Términos de referencia para participar del Salón Pacífico 2018</a>

                                    <h2>INVITADOS</h2>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Luis Ricaurte</p>
                                                        <p>(México)</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Asher  Remy Toledo</p>
                                                        <p>(Hyphen Hub, USA)</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Andrés Moreno Hoffman</p>
                                                        <p>(Casa Hoffman, Bogotá)</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Jaime Abraham Tamayo</p>
                                                        <p>(Curador México)</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Alejandro Castaño</p>
                                                        <p>(Coleccionista Bogotá)</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Mauricio Gómez</p>
                                                        <p>(Coleccionista Bogotá)</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Hernando Cardozo</p>
                                                        <p>(Coleccionista Bogotá)</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Miguel Saavedra</p>
                                                        <p>(Coleccionista Bogotá)</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Sandra Montenegro</p>
                                                        <p>(Coleccionista Bogotá)</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Alejandro Ferreira</p>
                                                        <p>(Coleccionista Bogotá)</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <h2>CONTENIDO</h2>
                                        <div class="col-md-12">
                                            <div><strong>CLAC: Exposición 15-20 artistas locales</strong></div>
                                            <div><strong>Premio CLAC: residencia en México</strong></div>
                                            <div><strong>Inauguración privada con invitados clave del mercado nacional e internacional</strong></div>
                                            <div><strong>Agenda académica</strong></div>
                                            <div><strong>San Antonio Distrito Artístico </strong></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <h2>AGENDA ACADÉMICA</h2>
                                        <div><strong>Coleccionismo en artes electrónicas</strong></div>
                                        <div><strong>Curaduría, investigación y mercados alternativos</strong></div>
                                        <div><strong>Coleccionismo en papel</strong></div>
                                        <div><strong>Gráfica contemporánea Post-Digital, Lasergrafía y nuevas tecnologías</strong></div>
                                        <div><strong>Coleccionismo Joven en Colombia</strong></div>
                                        <div><strong>Entre lo Afro y lo Mestizo</strong></div>
                                    </div>
                                    <div class="col-md-4">
                                        <h2>ARTISTAS CONVOCADOS</h2>
                                        <div><strong>Adrián Gaitán</strong></div>
                                        <div><strong>Sergio Zapata</strong></div>
                                        <div><strong>Andrés Velasco</strong></div>
                                        <div><strong>Connie Gutiérrez</strong></div>
                                        <div><strong>Herman Yusti</strong></div>
                                        <div><strong>Alejandra Gutiérrez</strong></div>
                                        <div><strong>Beatriz Grau</strong></div>
                                        <div><strong>Kurosh Sadhegian</strong></div>
                                        <div><strong>Verónica Lehner</strong></div>
                                        <div><strong>Wilson Nieva</strong></div>
                                        <div><strong>Edgar Jiménez</strong></div>
                                        <div><strong>Iván Tovar</strong></div>
                                        <div><strong>Juan Guillermo Quintero</strong></div>
                                        <div><strong>Juan Pablo Romero</strong></div>
                                        <div><strong>Colectivo Monómero</strong></div>
                                        <div><strong>Fabio Melecio Palacios</strong></div>
                                        <div><strong>Miguel Escobar</strong></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- .cd-project-info -->
                </li>

                <li>
                    <div class="preview-image">
                        <div class="cd-project-title">
                            <h2>AUDIOVISUAL</h2>
                            <!--<p>Brief description of the project here</p>-->
                        </div>
                    </div>
                    <div class="cd-project-info">
                        <div class="container info">
                            <div class="row">
                                <div class="col-md-12">
                                    <h2>OBJETIVOS</h2>
                                    <p>Fortalecimiento de empresas prestadoras de servicios de producción audiovisual, especialización en prestación de servicios y conexión con circuitos de demanda.</p>
                                    <h2>INVITADOS</h2>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Andrés Calderón</p>
                                                        <p>(Dynamo)</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Dago García</p>
                                                        <p>(Caracol)</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Rodrigo Guerrero</p>
                                                        <p>(AG Studio)</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Diego Ramírez</p>
                                                        <p>(64A Films)</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Ana Piñeres</p>
                                                        <p>(CMO Producciones)</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Congo Films, Le Cube</p>
                                                        <p>(Argentina)</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">PIXAR Latinoamérica</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">36 grados</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Noisey Colombia</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Madlove</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Timbo Animación</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">Black Velvet</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="userCards">
                                                    <div class="icon">
                                                        <img src="<?php bloginfo('template_url') ?>/assets/images/mic.png" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <p class="title">CineColor.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 "></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <h2>CONTENIDO</h2>
                                        <div class="col-md-12">
                                            <div><strong>1ra. muestra de servicios audiovisuales de Cali y el Valle del Cauca</strong></div>
                                            <div><strong>10 empresas locales presentan su pitch a 10 empresas nacionales</strong></div>
                                            <div><strong>Agenda Académica</strong></div>
                                            <div><strong>Networking Café</strong></div>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <h2>AGENDA ACADÉMICA</h2>
                                        <div class="col-md-6">
                                            <h3><strong>TALLERES</strong></h3>
                                            <div><strong>Los Best Boys: roles en el set de grabación</strong></div>
                                            <div><strong>La administración de una empresa audiovisual: Los ejecutivos y productores al  mando</strong></div>
                                        </div>
                                        <div class="col-md-6">
                                            <h3><strong>CHARLAS</strong></h3>
                                            <div><strong>El Negocio Audiovisual</strong></div>
                                            <div><strong>Cómo convertir un proyecto en empresa: El caso de la animación en Latinoamérica </strong></div>
                                            <div><strong>Los nuevos contenidos digitales: audiovisual y música </strong></div>
                                            <div><strong>una década audiovisual - Casos de éxito de empresas nacionales</strong></div>
                                            <div><strong>La Cara de los Proyectos Audiovisuales - Marketing para proyectos y empresas audiovisuales</strong></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- .cd-project-info -->
                </li>
            </ul> <!-- .cd-projects -->
            <!--<button class="scroll cd-text-replace">Scroll</button>-->
        </div> <!-- .cd-project-container -->
    </div>
</div>