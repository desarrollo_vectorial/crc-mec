<?php

$img = get_field( "imagen", "option" );
$titulo = get_field( "titulo", "option" );
$contenido = get_field( "contenido", "option" );

?>

<div class="row ml0 mr0">
    <div class="col-md-12 banner_home pt30 pb30 pl90 pr90" style="">
        <div class="col-md-5 p20">
            <?php if ($img) : ?>
                <img src="<?php echo $img; ?>" class="img-responsive">
            <?php else: ?>
                <img src="<?php bloginfo('template_url') ?>/assets/images/logo-mec.svg" class="img-responsive">
            <?php endif; ?>
        </div>
        <div class="col-md-7 pl30">
            <?php if ($titulo) : ?>
                <h2><?php echo $titulo; ?></h2>
            <?php else: ?>
                <h2><strong>FESTIVAL<br>DE CREATIVIDAD</strong><br>DEL 28 AL 30<br>NOV.</h2>
            <?php endif; ?>
            <?php if ($contenido) : ?>
                <p class="text-justify"><?php echo $contenido; ?></p>
            <?php else: ?>
                <p class="text-justify">Con el objetivo de darle continuidad al capítulo "Un Valle de Gente Creativa", de la narrativa estratégica de competitividad "Un Valle que se atreve", <strong>LA COMISIÓN REGIONAL DE COMPETITIVIDAD DEL VALLE DEL CAUCA PRESENTA EL MEC: MOVIMIENTO DE EMPRESAS CREATIVAS,</strong> un evento para el crecimiento de las industrias creativas de la región, cuyo objetivo es brindarle a los empresarios de Cali y el Valle del Cauca herramientas que les permitan fortalecer su modelo de negocio, conectarse con otros creativos y crecer.</p>
            <?php endif; ?>
        </div>
    </div>
</div>