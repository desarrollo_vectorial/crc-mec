<div class="row ml0 mr0">
    <div class="col-md-12 banner_3 p0">
        <div class="hide-on-m col-md-8 mapa p0" style="background: url(<?php bloginfo('template_url') ?>/assets/images/mapa.png)">
            <ul id="list-espacios">
                <li><a href="#" class="open-espacio icon-rosa" data-target="#espacio1" aria-controls="espacio1"></a></li>
                <li><a href="#" class="open-espacio icon-verde" data-target="#espacio2" aria-controls="espacio2"></a></li>
                <li><a href="#" class="open-espacio icon-verde" data-target="#espacio3" aria-controls="espacio3"></a></li>
                <li><a href="#" class="open-espacio icon-verde" data-target="#espacio4" aria-controls="espacio4"></a></li>
                <li><a href="#" class="open-espacio icon-verde" data-target="#espacio5" aria-controls="espacio5"></a></li>
                <li><a href="#" class="open-espacio icon-verde" data-target="#espacio6" aria-controls="espacio6"></a></li>
                <li><a href="#" class="open-espacio icon-verde" data-target="#espacio7" aria-controls="espacio7"></a></li>
                <li><a href="#" class="open-espacio icon-verde" data-target="#espacio8" aria-controls="espacio8"></a></li>
            </ul>
        </div>
        <div class="col-md-12 hide-on-d">
            <ul id="list-espacios-responsive">
                <li class="open-espacio" data-target="#espacio1" aria-controls="espacio1">
                    <a href="#" class="open-espacio icon-verde" data-target="#espacio1" aria-controls="espacio1"></a>
                    <h3><strong>CENTRO CULTURAL DE CALI</strong></h3>
                </li>
                <li class="open-espacio" data-target="#espacio2" aria-controls="espacio2">
                    <a href="#" class="open-espacio icon-verde" data-target="#espacio2" aria-controls="espacio2"></a>
                    <h3><strong>CASA MERCED</strong></h3>
                </li>
                <li class="open-espacio" data-target="#espacio3" aria-controls="espacio3">
                    <a href="#" class="open-espacio icon-verde" data-target="#espacio3" aria-controls="espacio3"></a>
                    <h3><strong>CÁMARA DE COMERCIO DE CALI</strong></h3>
                </li>
                <li class="open-espacio" data-target="#espacio4" aria-controls="espacio4">
                    <a href="#" class="open-espacio icon-verde" data-target="#espacio4" aria-controls="espacio4"></a>
                    <h3><strong>BANCO DE LA REPÚBLICA</strong></h3>
                </li>
                <li class="open-espacio" data-target="#espacio5" aria-controls="espacio5">
                    <a href="#" class="open-espacio icon-verde" data-target="#espacio5" aria-controls="espacio5"></a>
                    <h3><strong>CENTRO CULTURAL DE COMFANDI</strong></h3>
                </li>
                <li class="open-espacio" data-target="#espacio6" aria-controls="espacio6">
                    <a href="#" class="open-espacio icon-verde" data-target="#espacio6" aria-controls="espacio6"></a>
                    <h3><strong>MANGO TREE</strong></h3>
                </li>
                <li class="open-espacio" data-target="#espacio7" aria-controls="espacio7">
                    <a href="#" class="open-espacio icon-verde" data-target="#espacio7" aria-controls="espacio7"></a>
                    <h3><strong>TEATRINO DEL TEATRO MUNICIPAL DE CALI</strong></h3>
                </li>
                <li class="open-espacio" data-target="#espacio8" aria-controls="espacio8">
                    <a href="#" class="open-espacio icon-verde" data-target="#espacio8" aria-controls="espacio8"></a>
                    <h3><strong>CASA PROARTES</strong></h3>
                </li>
            </ul>
        </div>
        <div class="col-md-4 p0 content-espacios">
            <div id="espacio1" class="is-visible">
                <div class="col-md-12 text-center">
                    <div class="col-md-12 text-right hide-on-d">
                        <a class="close-espacios" href="#"><i class="fa fa-chevron-left"></i></a>
                    </div>
                    <h3><strong>CENTRO CULTURAL DE CALI</strong></h3>
                    <img src="<?php bloginfo('template_url') ?>/assets/images/centro_cultural.png" class="img-responsive">
                    <p class="text-justify mt10 mb10">El edificio Centro Cultural de Cali es la sede de la Secretaría de Cultura y Turismo, Corfecali y el archivo histórico de la ciudad. Se encuentra ubicado en el centro histórico de Cali, junto al Teatro Municipal Enrique Buenaventura.</p>
                    <div class="col-md-6">
                        <a href="https://www.google.com/maps/dir//Centro+Cultural+De+Cali,+Cra.+5+%23%236-05,+Cali,+Valle+del+Cauca/@3.4493235,-76.5377007,17z/data=!4m9!4m8!1m0!1m5!1m1!1s0x8e30a664ba936d51:0x18d4bfcd0909c87f!2m2!1d-76.5364262!2d3.4495219!3e0" class="btn btn-espacios" target="_blank">¿CÓMO LLEGAR?</a>
                    </div>
                    <div class="col-md-6">
                        <a href="http://crcvalle.org.co/mec/wp-content/uploads/2018/10/mec_centro_cultural.pdf" class="btn btn-espacios" target="_blank">PROGRAMACIÓN</a>
                    </div>
                </div>
            </div>
            <div id="espacio2">
                <div class="col-md-12 text-center">
                    <div class="col-md-12 text-right hide-on-d">
                        <a class="close-espacios" href="#"><i class="fa fa-chevron-left"></i></a>
                    </div>
                    <h3><strong>CASA MERCED</strong></h3>
                    <img src="<?php bloginfo('template_url') ?>/assets/images/merced.png" class="img-responsive">
                    <p class="text-justify mt10 mb10">Centro ideal para eventos sociales, corporativos y culturales en una casona colonial dotada con todas las comodidades modernas.</p>
                    <div class="col-md-6">
                        <a href="https://www.google.com/maps/dir//Casa+Merced,+Carrera+4,+Cali,+Valle+del+Cauca/@3.4502921,-76.5714342,13z/data=!3m1!4b1!4m9!4m8!1m0!1m5!1m1!1s0x8e30a7c38ebb4be9:0xa6476d9f8a087a38!2m2!1d-76.5364147!2d3.4502071!3e0" class="btn btn-espacios" target="_blank">¿CÓMO LLEGAR?</a>
                    </div>
                    <div class="col-md-6">
                        <a href="http://crcvalle.org.co/mec/wp-content/uploads/2018/10/mec_merced.pdf" class="btn btn-espacios" target="_blank">PROGRAMACIÓN</a>
                    </div>
                </div>
            </div>
            <div id="espacio3">
                <div class="col-md-12 text-center">
                    <div class="col-md-12 text-right hide-on-d">
                        <a class="close-espacios" href="#"><i class="fa fa-chevron-left"></i></a>
                    </div>
                    <h3><strong>CÁMARA DE COMERCIO DE CALI</strong></h3>
                    <img src="<?php bloginfo('template_url') ?>/assets/images/camara_comercio.png" class="img-responsive">
                    <p class="text-justify mt10 mb10">Entidad privada de carácter corporativo, gremial y sin ánimo de lucro, que trabaja por una región mas próspera que genere mejor calidad de vida para sus habitantes.</p>
                    <div class="col-md-6">
                        <a href="https://www.google.com/maps/dir//C%C3%A1mara+de+Comercio+de+Cali+Kr9+Cl21,+Carrera+9,+Cali,+Valle+del+Cauca/@3.45309,-76.5263986,15.19z/data=!4m9!4m8!1m0!1m5!1m1!1s0x8e30a65c52709401:0x28844e7166fc2def!2m2!1d-76.52258!2d3.450312!3e0" class="btn btn-espacios" target="_blank">¿CÓMO LLEGAR?</a>
                    </div>
                    <div class="col-md-6">
                        <a href="http://crcvalle.org.co/mec/wp-content/uploads/2018/10/mec_centro_camara_de_comercio.pdf" class="btn btn-espacios" target="_blank">PROGRAMACIÓN</a>
                    </div>
                </div>
            </div>
            <div id="espacio4">
                <div class="col-md-12 text-center">
                    <div class="col-md-12 text-right hide-on-d">
                        <a class="close-espacios" href="#"><i class="fa fa-chevron-left"></i></a>
                    </div>
                    <h3><strong>BANCO DE LA REPÚBLICA</strong></h3>
                    <img src="<?php bloginfo('template_url') ?>/assets/images/banco_republica_imagen.png" class="img-responsive">
                    <p class="text-justify mt10 mb10">Banco central de la República de Colombia, entidad creada por medio de la ley 252 de 1923 y encargada de emitir, manejar y controlar los movimientos monetarios de Colombia así como emitir la moneda de curso legal en el país, el peso.</p>
                    <div class="col-md-6">
                        <a href="https://www.google.com/maps/dir//Banco+de+la+Rep%C3%BAblica,+Cali,+Valle+del+Cauca/@3.4497368,-76.5345261,15.86z/data=!4m9!4m8!1m0!1m5!1m1!1s0x8e30a664e59aa569:0x4bd5ef021ed7c78d!2m2!1d-76.5353181!2d3.4505111!3e0" class="btn btn-espacios" target="_blank">¿CÓMO LLEGAR?</a>
                    </div>
                    <div class="col-md-6">
                        <a href="http://crcvalle.org.co/mec/wp-content/uploads/2018/10/mec_banco_republica.pdf" class="btn btn-espacios" target="_blank">PROGRAMACIÓN</a>
                    </div>
                </div>
            </div>
            <div id="espacio5">
                <div class="col-md-12 text-center">
                    <div class="col-md-12 text-right hide-on-d">
                        <a class="close-espacios" href="#"><i class="fa fa-chevron-left"></i></a>
                    </div>
                    <h3><strong>CENTRO CULTURAL DE COMFANDI</strong></h3>
                    <img src="<?php bloginfo('template_url') ?>/assets/images/centro_cultural_comfandi.png" class="img-responsive">
                    <p class="text-justify mt10 mb10">Lugar que permite a la comunidad realizar sus actividades artísticas, culturales, educativas y empresariales.</p>
                    <div class="col-md-6">
                        <a href="https://www.google.com/maps/dir//Centro+Cultural+Comfandi,+Carrera+6,+Cali,+Valle+del+Cauca/@3.4491993,-76.5696174,13z/data=!3m1!4b1!4m9!4m8!1m0!1m5!1m1!1s0x8e30a6648642e841:0x54954be67666e642!2m2!1d-76.5345979!2d3.4491143!3e0" class="btn btn-espacios" target="_blank">¿CÓMO LLEGAR?</a>
                    </div>
                    <div class="col-md-6">
                        <a href="http://crcvalle.org.co/mec/wp-content/uploads/2018/10/mec_centro_cultural_comfandi.pdf" class="btn btn-espacios" target="_blank">PROGRAMACIÓN</a>
                    </div>
                </div>
            </div>
            <div id="espacio6">
                <div class="col-md-12 text-center">
                    <div class="col-md-12 text-right hide-on-d">
                        <a class="close-espacios" href="#"><i class="fa fa-chevron-left"></i></a>
                    </div>
                    <h3><strong>MANGO TREE</strong></h3>
                    <img src="<?php bloginfo('template_url') ?>/assets/images/mango_tree.png" class="img-responsive">
                    <p class="text-justify mt10 mb10">El Mango Tree Cali es un hotel que se encuentra en Cali, a 600 metros de la catedral de San Pedro, y ofrece alojamiento con WiFi gratuita.</p>
                    <div class="col-md-6">
                        <a href="https://www.google.com.co/maps/place/MANGO+TREE+HOSTEL/@3.4497657,-76.5377957,17.78z/data=!4m7!3m6!1s0x8e30a664bb7c9af9:0x5656401f488bf17e!5m1!1s2018-10-28!8m2!3d3.449454!4d-76.536269" class="btn btn-espacios" target="_blank">¿CÓMO LLEGAR?</a>
                    </div>
                    <!--<div class="col-md-6">
                        <a href="http://crcvalle.org.co/mec/wp-content/uploads/2018/10/mec_centro_cultural_comfandi.pdf" class="btn btn-espacios" target="_blank">PROGRAMACIÓN</a>
                    </div>-->
                </div>
            </div>
            <div id="espacio7">
                <div class="col-md-12 text-center">
                    <div class="col-md-12 text-right hide-on-d">
                        <a class="close-espacios" href="#"><i class="fa fa-chevron-left"></i></a>
                    </div>
                    <h3><strong>TEATRINO DEL TEATRO MUNICIPAL DE CALI</strong></h3>
                    <img src="<?php bloginfo('template_url') ?>/assets/images/teatrino.png" class="img-responsive">
                    <p class="text-justify mt10 mb10">Es un teatro declarado monumento nacional en 1982 que se encuentra en el centro de la ciudad de Santiago de Cali</p>
                    <div class="col-md-6">
                        <a href="https://www.google.com.co/maps/place/Teatro+Municipal+Enrique+Buenaventura/@3.4492648,-76.5374871,17.3z/data=!4m5!3m4!1s0x8e30a6635697f7f5:0xf3d550b97e5edb57!8m2!3d3.4493542!4d-76.5360819" class="btn btn-espacios" target="_blank">¿CÓMO LLEGAR?</a>
                    </div>
                    <!--<div class="col-md-6">
                        <a href="http://crcvalle.org.co/mec/wp-content/uploads/2018/10/mec_centro_cultural_comfandi.pdf" class="btn btn-espacios" target="_blank">PROGRAMACIÓN</a>
                    </div>-->
                </div>
            </div>
            <div id="espacio8">
                <div class="col-md-12 text-center">
                    <div class="col-md-12 text-right hide-on-d">
                        <a class="close-espacios" href="#"><i class="fa fa-chevron-left"></i></a>
                    </div>
                    <h3><strong>CASA PROARTES</strong></h3>
                    <img src="<?php bloginfo('template_url') ?>/assets/images/proartes.png" class="img-responsive">
                    <p class="text-justify mt10 mb10">Es un teatro declarado monumento nacional en 1982 que se encuentra en el centro de la ciudad de Santiago de Cali.</p>
                    <div class="col-md-6">
                        <a href="https://www.google.com.co/maps/place/Casa+Proartes/@3.4490654,-76.5359642,17.53z/data=!4m5!3m4!1s0x0:0xefe3a09f8f68dc6f!8m2!3d3.4497146!4d-76.535702" class="btn btn-espacios" target="_blank">¿CÓMO LLEGAR?</a>
                    </div>
                    <!--<div class="col-md-6">
                        <a href="http://crcvalle.org.co/mec/wp-content/uploads/2018/10/mec_centro_cultural_comfandi.pdf" class="btn btn-espacios" target="_blank">PROGRAMACIÓN</a>
                    </div>-->
                </div>
            </div>
        </div>
    </div>
</div>