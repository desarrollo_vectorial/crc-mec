var $ = jQuery;
$(document).ready(function() {
    $('#horizontal-scroll').pagepiling({
        direction: 'horizontal',
        menu: '#leftnavbar',
        anchors: ['page1', 'page2', 'page3', 'page4'],
        sectionsColor: ['white', 'white', 'white', 'white'],
        navigation: false,
        AllowScrolling: false,
        KeyboardScrolling: false,
        normalScrollElementTouchThreshold: 15,
        normalScrollElements: '#section1, #section2, #section3, #section4',
        afterRender: function(){
            $('.pp-tableCell').addClass('pp-tableCell-custom');
            var h = $('.banner_home').innerHeight();
            var h2 = $('.banner_2').innerHeight();
            var h3 = $('.banner_3').innerHeight();
            var h4 = $('.banner_4').innerHeight();
            //console.log(h);
            $('#horizontal-scroll').css( "min-height", h+'px');
            $('.mapa, .content-espacios').css("min-height", h3 + 'px');

            $('#leftnavbar li').click(function() {
                var data = $(this).attr('data-menuanchor');
                $('.cd-projects-previews').find('li.slide-out').removeClass('slide-out');
                $('.cd-projects').find('li.selected').removeClass('content-visible selected');
                $('.cd-projects-container, .cd-nav-trigger').removeClass('project-open');
                //$container.cycle(id.replace('#', ''));
                //return false;
                //console.log(data);
                if (data == 'page1') {
                    $('#horizontal-scroll').css("min-height", h + 'px');
                }else {

                    if (data == 'page2') {
                        $('#horizontal-scroll').css("min-height", h2 + 'px');
                    } else if (data == 'page3') {
                        $('#horizontal-scroll').css("min-height", h3 + 'px');
                    } else if (data == 'page4') {
                        $('#horizontal-scroll').css("min-height", h4 + 'px');
                    }
                }
            });

            $(".js-open").on("click", function () {
                var target = $(this).attr("data-target");
                //console.log(target);
                $(target).toggleClass("is-visible");
                $('.customoverlay').addClass('showoverlay');
            });

            $(".open-espacio").on("click", function () {
                var target = $(this).attr("data-target");
                var parent = $(this).parent();
                var hermanos = $(parent).siblings().children();
                $(hermanos).removeClass("icon-rosa").addClass("icon-verde");
                $(this).addClass("icon-rosa").removeClass("icon-verde");
                $('h3').removeClass("icon-verde");
                $('li.open-espacio').removeClass("icon-rosa");
                $(target).siblings().removeClass("is-visible");
                $(target).addClass("is-visible");
            });

            $( ".close-espacios" ).click(function() {
                $("#espacio1,#espacio2,#espacio3,#espacio4,#espacio5").removeClass("is-visible");
            });

            $( ".close_sidebar, .customoverlay" ).click(function() {
                $("#nav-sidebar").removeClass("is-visible");
                $('.customoverlay').removeClass('showoverlay');
            });

            if ($(window).width() < 960) {
                $("#espacio1").removeClass("is-visible");
            }

            $('.owl-carousel').owlCarousel({
                loop:false,
                margin:10,
                nav:true,
                dots:false,
                responsiveClass:true,
                navText: ['<i class="fa fa-chevron-left"></i>','<i class="fa fa-chevron-right"></i>'],
                responsive:{
                    0:{
                        items:1,
                        nav:true
                    },
                    600:{
                        items:3,
                        nav:true
                    },
                    1200:{
                        items:4,
                        nav:true
                    }
                }
            });
            var projectsContainer = $('.cd-projects-container'),
                projectsPreviewWrapper = projectsContainer.find('.cd-projects-previews'),
                projectPreviews = projectsPreviewWrapper.children('li'),
                projects = projectsContainer.find('.cd-projects'),
                navigationTrigger = $('.cd-nav-trigger'),
                project = {};

            function openProject(projectPreview) {
                var projectIndex = projectPreview.index();
                var projectSelected = projects.children('li').eq(projectIndex).add(projectPreview);
                var projectHeight = $(projectSelected).find('.cd-project-info').innerHeight();
                project.height = projectHeight;
            }

            projectsPreviewWrapper.on('click', 'a', function(event){
                event.preventDefault();
                openProject($(this).parent('li'));
                $('#horizontal-scroll, .banner_4').css("min-height", project.height + 'px');
                if ($(window).width() < 960) {
                    $('#horizontal-scroll, .banner_4').css("min-height", project.height + 407 + 'px');
                }
            });
            navigationTrigger.on('click', function(event){
                event.preventDefault();
                $('#horizontal-scroll, .banner_4').css("min-height", '');
            });
        },
        afterLoad: function(anchorLink, index){
            if(index>1){
                $('#pp-nav').removeClass('custom');
            }else{
                $('#pp-nav').addClass('custom');
            }
        }
    });
});