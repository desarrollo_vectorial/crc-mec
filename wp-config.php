<?php

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor - Security > Settings > WordPress Tweaks > File Editor
// END iThemes Security - Do not modify or remove this line

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

define('HTTP_HOST', $_SERVER["HTTP_HOST"]);
define('LOCAL_DOMAIN', 'ccc.mec.local');
define('LOCAL_DOMAIN2', 'mec.192.168.0.21.xip.io');
define('PROD_DOMAIN', 'crcvalle.org.co');
define('PROD_DOMAIN2', 'www.crcvalle.org.co');

switch (HTTP_HOST) {
    case LOCAL_DOMAIN:
        // ** MySQL settings - You can get this info from your web host ** //
        /** El nombre de tu base de datos de WordPress */
        define('WP_HOME','http://ccc.mec.local/');
        define('WP_SITEURL','http://ccc.mec.local/');
        /** El nombre de tu base de datos de WordPress */
        define('DB_NAME', 'dev_mec_landing');
        /** Tu nombre de usuario de MySQL */
        define('DB_USER', 'root');
        /** Tu contraseña de MySQL */
        if ( substr(PHP_OS, 0, 3) === 'WIN' ) {
            define('DB_PASSWORD', '');
        }
        else {
            define('DB_PASSWORD', 'root');
        }
        /** Host de MySQL (es muy probable que no necesites cambiarlo) */
        define('DB_HOST', 'localhost');
        break;

    case LOCAL_DOMAIN2:
        // ** MySQL settings - You can get this info from your web host ** //
        /** El nombre de tu base de datos de WordPress */
        define('WP_HOME','http://mec.192.168.0.21.xip.io/');
        define('WP_SITEURL','http://mec.192.168.0.21.xip.io/');
        /** El nombre de tu base de datos de WordPress */
        define('DB_NAME', 'dev_mec_landing');
        /** Tu nombre de usuario de MySQL */
        define('DB_USER', 'root');
        /** Tu contraseña de MySQL */
        if ( substr(PHP_OS, 0, 3) === 'WIN' ) {
            define('DB_PASSWORD', '');
        }
        else {
            define('DB_PASSWORD', 'root');
        }
        /** Host de MySQL (es muy probable que no necesites cambiarlo) */
        define('DB_HOST', 'localhost');
        break;
    case PROD_DOMAIN:
        // ** MySQL settings - You can get this info from your web host ** //
        /** El nombre de tu base de datos de WordPress */
        define('WP_HOME','http://crcvalle.org.co/mec/');
        define('WP_SITEURL','http://crcvalle.org.co/mec/');
        define('DB_NAME', 'webcrc_w3bs1t3');
        /** Tu nombre de usuario de MySQL */
        define('DB_USER', 'webcrc_w3bs1t3');
        /** Tu contraseña de MySQL */
        define('DB_PASSWORD', 'crcv4ll3w3bs1t3');
        /** Host de MySQL (es muy probable que no necesites cambiarlo) */
        define('DB_HOST', '127.0.0.1');
        break;

    case PROD_DOMAIN2:
        // ** MySQL settings - You can get this info from your web host ** //
        /** El nombre de tu base de datos de WordPress */
        define('WP_HOME','http://www.crcvalle.org.co/mec/');
        define('WP_SITEURL','http://www.crcvalle.org.co/mec/');
        define('DB_NAME', 'webcrc_w3bs1t3');
        /** Tu nombre de usuario de MySQL */
        define('DB_USER', 'webcrc_w3bs1t3');
        /** Tu contraseña de MySQL */
        define('DB_PASSWORD', 'crcv4ll3w3bs1t3');
        /** Host de MySQL (es muy probable que no necesites cambiarlo) */
        define('DB_HOST', '127.0.0.1');
        break;
}

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'eh9P>UG.eRB%5`B[0^~{OupVJg1;2K^|M&T8%3c],}Hyh3~+?gd^E}]Gw,mg|r37');
define('SECURE_AUTH_KEY',  'tr76g{Tkv.[cJw5kW$y:`zt<nR.;w*Aq<+;4V=kq^5Gd]i!iOr.*dx?<4mqV<LD0');
define('LOGGED_IN_KEY',    '%|wT$ymVWwLv<R8*8fb+.(UBPp%j=ef E8~=eg$YiuciWu2:Jg@PFs^0G,r;qR]*');
define('NONCE_KEY',        'Fi1.%:*<o~|kTfa*uA82Lqvwg0Ta<; /0WgZ>-5xA7=,`qJ5`fmy[r6>!jJgN uC');
define('AUTH_SALT',        'wr9|][Zn3#o@%V,lIovJJmtQ0vfW.}Is ^r{KdIT3U*-^1 emViK2fWl?qVrxW-#');
define('SECURE_AUTH_SALT', ',20`=ER:$u_O h0f)E<-j;]%$Ajyz,!OD&9S99bp<X#d1FZdlv]oppmmN d<*c/h');
define('LOGGED_IN_SALT',   '+;`)!v+4VKP{kh%lH$b}I_&hS:3I28_-. 5(we[k7i^Us)I[q| qHCF,,?G]0n;u');
define('NONCE_SALT',       'O)E><==dP4>WQ6kX{/]k:qG|dZgxK6!=w& wgR|{=/MV,)L*P%6$ymrMqCxQXZxI');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'mec_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
